# Blog from scratch

Développement en PHP / MySQL / HTML / CSS d'un blog. From scratch ! Aucun Framework / CMS / Librairie autorisé.

# Contexte du projet

- Créer un blog de A à Z, ce n'est pas sorcier !

# Maquettage

Temps de réalisation: 2 à 4h

Réaliser les wireframes nécessaires à la réalisation du blog.

Ce blog devrait permettre aux utilisateurs :

- D'accéder à la liste des billets de blog Cette liste affichera pour chaque billet le titre, un visuel, les 300 premiers caractères du contenu, l'auteur, la date de publication, les catégories auxquelles il appartient, le temps de lecture (en minute) et un lien "Lire la suite".
- De lire un billet de blog Cette page affichera pour un billet le titre, un visuel, le contenu du billet, l'auteur, la date de publication, les catégories auxquelles ils appartiennent.

Toutes les pages du blog devront afficher le titre du blog "Blog from scratch", le logo du blog, et un copyright "© {Année au format YYYY} blogfromscratch".


# Conception de la base de données

Temps de réalisation: 2 à 4h

Ressource(s): - Wireframes de l'application

- Réaliser un Diagramme Use-Case
- Réaliser un MCD (modèle conceptuel de données) nécessaire à la mise en place de la base de données du blog.

# Mise en place de la base de données

Temps de réalisation: 1 à 2h

_Ressource(s): _

- MCD

Réaliser un script SQL permettant la création des tables MySQL définit par le MCD.

# Développement de l'interface web statique

Temps de réalisation: 4h à 1j

Ressource(s):

- Wireframes de l'application

A partir des wireframes, réaliser les pages HTML statiques du blog avec des données factices (3 billets de blog) 

# Développement de l'interface web dynamique

Temps de réalisation: 1 à 2j

Ressource(s):

- Scripts SQL de création de la base de données et de remplissage de la base avec des données factices (les données factices peuvent être rentrées manuellement par les apprenants)
- Code source de l'interface web statique

Rendre l'interface dynamique en récupérant les billets de blog présent dans la base de données.

La page d'accueil devra lister les billets du plus récent au plus anciens, il sera possible de filtrer cette liste par auteurs et/ou par catégorie. Depuis la page d'un billet de blog, un rebond doit être possible sur l'auteurs et sur les catégories.

# Un peu de sécurité

Temps de réalisation: 4h à 1j

Les auteurs doivent pouvoir se connecter à la plateforme à l'aide d'une adresse mail et d'un mot de passe. Les billets de blog doivent avoir un statut de publication et les auteurs connectés doivent voir apparaître les billets de blog non publié dont ils sont l'auteur.

Une personne malveillante ayant accès à la base de données ne doit pas pouvoir récupérer les mots de passe des utilisateurs. Une personne malveillante ayant accès au code source du projet ne doit pas pouvoir récupérer les mots de passe des utilisateurs.

# Un peu d'éco-conception 

Temps de réalisation: indéterminé

Réduire au maximum l'empreinte carbone de la plateforme.

# Un peu d'accessibilité

Temps de réalisation: indéterminé

Corriger les défauts d’accessibilité des développements réalisés (Il ne devrait pas en avoir !).

# Encore un petit bonus

Temps de réalisation: indéterminé

Ajouter une fonctionnalité de recherche.

Si le terme recherché apparait dans le titre, le billet de blog remonte en premier. Si le terme recherché apparait dans les catégories, le billet de blog remonte en deuxième. Si le terme recherché apparait dans les contenu de l'article, le billet de blog remonte en troisième.

# Modalités pédagogiques

2 à 5j